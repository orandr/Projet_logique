import java.io.*;
public class Salut_Systeme {
    public static class AfficheurFlux implements Runnable {
        private final InputStream inputstream;
        //Cosntructeur
        AfficheurFlux(InputStream inputstream){
            this.inputstream = inputstream;
        }
        private BufferedReader getBuff(InputStream input){
            return new BufferedReader(new InputStreamReader(input) );
        }
        public void run() {
            BufferedReader br = getBuff( inputstream );
            String ligne = "";
            try{
                while((ligne=br.readLine())!=null){
                    System.out.println(ligne);
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }  
        public static final String CHEMIN ="./";
        public static int exec(String cmd){
            int cr = 0;
            try{
                 ProcessBuilder pb = new ProcessBuilder ("sh","-c",cmd); 
                 pb.directory(new File(CHEMIN));
                 Process p = pb.start();
                 AfficheurFlux fluxsortie = new AfficheurFlux(p.getInputStream());
                 AfficheurFlux fluxErreur = new AfficheurFlux(p.getErrorStream());
                 new Thread(fluxsortie).start();
                 new Thread(fluxErreur).start();
                 cr = p.waitFor();
                
            }catch (IOException e) {
                e.printStackTrace();
            }catch (InterruptedException e) {
                e.printStackTrace();
            }
            return cr;
        }
}
