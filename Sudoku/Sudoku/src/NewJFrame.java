import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import javax.swing.JTextField;
import java.io.*;
import javax.swing.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author chouaib
 * @version 09-03-2018
 * @
 */
public class NewJFrame extends javax.swing.JFrame {

//ATTRIBUTS ----- reste des attributs declarés en bas -----
private Sudoku sdk;
private final int PERCENT = 80;
private javax.swing.JButton[][] grid;
private javax.swing.JButton[][] grid4;
private javax.swing.JButton[][] grid9;
private javax.swing.JButton[][] grid16;
private javax.swing.JButton[][] grid25;
private int niv;
private String strperso;
private javax.swing.JPanel paneGrid;
private javax.swing.JPanel paneGrid4;
private javax.swing.JPanel paneGrid9;
private javax.swing.JPanel paneGrid16;
private javax.swing.JPanel paneGrid25;
    
    public NewJFrame() {
        initMyComponents();
        lancement();
        gridColor();
        gridColor4();
        gridColor9();
        gridColor16();
        gridColor25();
    }
    
    public void initGrid(){
        for (int i=0; i<sdk.taille ;i++){
            for (int j=0; j<sdk.taille ;j++){
                grid[i][j].setText("");
                grid[i][j].setFont(new java.awt.Font("Tw Cen MT", 1, 19));
            }
        }
    }
    
    public void gridColor(){
        for (int i=0; i<9 ;i++){
            for (int j=0; j<9 ;j++){
                //grid[i][j].setBorderPainted(false);
                grid[i][j].setFocusPainted(false);
                if((i==0 || i==1 || i==2 ||i==6 || i==7 || i==8) && ( j==0 || j==1 || j==2 || j==6 || j==7 || j==8)){ 
                    grid[i][j].setBackground(Color.decode("#d2d4dc"));
                }
                else if (( i==3 || i==4 || i==5) && (j==3 || j==4 || j==5)){
                    grid[i][j].setBackground(Color.decode("#d2d4dc"));  //"#d2d4dc"
                }
                else{
                    grid[i][j].setBackground(Color.decode("#00bcd4"));  //"#00bcd4"
                }
            }
        }
    }
    
    public void gridColor4(){
        for (int i=0; i<4 ;i++){
            for (int j=0; j<4 ;j++){
                //grid[i][j].setBorderPainted(false);
                grid4[i][j].setFocusPainted(false);
                if((i==0 || i==1) && ( j==0 || j==1)){ 
                    grid4[i][j].setBackground(Color.decode("#d2d4dc"));
                }
                else if (( i==2 || i==3) && (j==2 || j==3)){
                    grid4[i][j].setBackground(Color.decode("#d2d4dc"));  //"#d2d4dc"
                }
                else{
                    grid4[i][j].setBackground(Color.decode("#00bcd4"));  //"#00bcd4"
                }
            }
        }
    }
    
    public void gridColor9(){
        for (int i=0; i<9 ;i++){
            for (int j=0; j<9 ;j++){
                //grid[i][j].setBorderPainted(false);
                grid9[i][j].setFocusPainted(false);
                if((i==0 || i==1 || i==2 ||i==6 || i==7 || i==8) && ( j==0 || j==1 || j==2 || j==6 || j==7 || j==8)){ 
                    grid9[i][j].setBackground(Color.decode("#d2d4dc"));
                }
                else if (( i==3 || i==4 || i==5) && (j==3 || j==4 || j==5)){
                    grid9[i][j].setBackground(Color.decode("#d2d4dc"));  //"#d2d4dc"
                }
                else{
                    grid9[i][j].setBackground(Color.decode("#00bcd4"));  //"#00bcd4"
                }
            }
        }
    }
    
    public void gridColor16(){
        for (int i=0; i<16 ;i++){
            for (int j=0; j<16 ;j++){
                //grid[i][j].setBorderPainted(false);
                grid16[i][j].setFocusPainted(false);
                if((i==0 || i==1 || i==2 || i==3 ||i==8 || i==9 || i==10 || i==11) && (j==0 || j==1 || j==2 || j==3 ||j==8 || j==9 || j==10 || j==11)){ 
                    grid16[i][j].setBackground(Color.decode("#d2d4dc"));
                }
                else if (( i==4 || i==5 || i==6 || i==7 || i==12 || i==13 || i==14 || i==15) && ( j==4 || j==5 || j==6 || j==7 || j==12 || j==13 || j==14 || j==15)){
                    grid16[i][j].setBackground(Color.decode("#d2d4dc"));  //"#d2d4dc"
                }
                else{
                    grid16[i][j].setBackground(Color.decode("#00bcd4"));  //"#00bcd4"
                }
            }
        }
    }
    
    public void gridColor25(){
        for (int i=0; i<25 ;i++){
            for (int j=0; j<25 ;j++){
                //grid[i][j].setBorderPainted(false);
                grid25[i][j].setFocusPainted(false);
                if((i==0 || i==1 || i==2 || i==3 || i==4 ||i==10 || i==11 || i==12 || i==13 ||i==14 || i==20 || i==21 || i==22 || i==23 || i==24)
                && (j==0 || j==1 || j==2 || j==3 || j==4 ||j==10 || j==11 || j==12 || j==13 ||j==14 || j==20 || j==21 || j==22 || j==23 || j==24)){ 
                    grid25[i][j].setBackground(Color.decode("#d2d4dc"));
                }
                else if ((i==5 || i==6 || i==7 || i==8 || i==9 ||i==15 || i==16 || i==17 || i==18 ||i==19)
                && (j==5 || j==6 || j==7 || j==8 || j==9 ||j==15 || j==16 || j==17 || j==18 ||j==19)){
                    grid25[i][j].setBackground(Color.decode("#d2d4dc"));  //"#d2d4dc"
                }
                else{
                    grid25[i][j].setBackground(Color.decode("#00bcd4"));  //"#00bcd4"
                }
            }
        }
    }
    
    public void remplirGrid(){
        for (int i=0; i<sdk.taille ;i++){
            for (int j=0; j<sdk.taille ;j++){
                grid[i][j].setFont(new java.awt.Font("tahoma", 1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid[i][j].setText("");
                }
                else{
                       grid[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid[i][j].setForeground(Color.black);
                }
            }
        }
    }
     
    public void remplirGrid4(){
        for (int i=0; i<4 ;i++){
            for (int j=0; j<4 ;j++){
                grid4[i][j].setFont(new java.awt.Font("tahoma", 1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid4[i][j].setText("");
                }
                else{
                       grid4[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid4[i][j].setForeground(Color.black);
                }
            }
        }
    }
    
    public void remplirGrid9(){
        for (int i=0; i<9 ;i++){
            for (int j=0; j<9 ;j++){
                grid9[i][j].setFont(new java.awt.Font("tahoma", 1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid9[i][j].setText("");
                }
                else{
                       grid9[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid9[i][j].setForeground(Color.black);
                }
            }
        }
    }
    
    public void remplirGrid16(){
        for (int i=0; i<16 ;i++){
            for (int j=0; j<16 ; j++){
                grid16[i][j].setFont(new java.awt.Font("tahoma", 1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid16[i][j].setText("");
                }
                else{
                       grid16[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid16[i][j].setForeground(Color.black);
                }
            }
        }
    }
    
    
    public void remplirGrid25(){
        for (int i=0; i<25 ;i++){
            for (int j=0; j<25 ; j++){
                grid25[i][j].setFont(new java.awt.Font("tahoma", 1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid25[i][j].setText("");
                }
                else{
                       grid25[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid25[i][j].setForeground(Color.black);
                }
            }
        }
    }
    
    
     public void Gridsol(){
        for (int i=0; i<sdk.taille ;i++){
            for (int j=0; j<sdk.taille ;j++){
                grid[i][j].setFont(new java.awt.Font("tahoma",1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid[i][j].setText("");
                }
                else{
                       grid[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid[i][j].setForeground(Color.red);
                }
            }
        }
    }
     
     public void Gridsol4(){
        for (int i=0; i<4 ;i++){
            for (int j=0; j<4 ;j++){
                grid4[i][j].setFont(new java.awt.Font("tahoma",1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid4[i][j].setText("");
                }
                else{
                       grid4[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid4[i][j].setForeground(Color.red);
                }
            }
        }
    }
     
     public void Gridsol9(){
        for (int i=0; i<9 ;i++){
            for (int j=0; j<9 ;j++){
                grid9[i][j].setFont(new java.awt.Font("tahoma",1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid9[i][j].setText("");
                }
                else{
                       grid9[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid9[i][j].setForeground(Color.red);
                }
            }
        }
    }
     
     public void Gridsol16(){
        for (int i=0; i<16 ; i++){
            for (int j=0; j<16 ; j++ ) {
                grid16[i][j].setFont(new java.awt.Font("tahoma",1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid16[i][j].setText("");
                }
                else{
                       grid16[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid16[i][j].setForeground(Color.red);
                }
            }
        }
    }
    
     public void Gridsol25(){
        for (int i=0; i<25;i++){
            for (int j=0; j<25 ; j++){
                grid25[i][j].setFont(new java.awt.Font("tahoma",1, 18));
                if(sdk.sudoku[i][j] ==0) {
                       grid25[i][j].setText("");
                }
                else{
                       grid25[i][j].setText(""+sdk.sudoku[i][j]+"");
                       grid25[i][j].setForeground(Color.red);
                }
            }
        }
    }
     
    private void lancement(){
        next.setVisible(false);
        back.setVisible(false);
        stringsudoku.setVisible(false);
        logomini.setVisible(false);
        menu.setVisible(false);
        solve.setVisible(false);
        clear.setVisible(false); 
        back2.setVisible(false); 
        paper.setVisible(false); 
        intro.setVisible(false); 
        paneGrid.setVisible(false);
        paneGrid4.setVisible(false);
        paneGrid9.setVisible(false);
        paneGrid16.setVisible(false);
        paneGrid25.setVisible(false);
        infos.setVisible(false);
        
        //sdk = new Sudoku();
    }
    
     private void initMyComponents() {

        paneMain = new javax.swing.JPanel();
        logomini = new javax.swing.JLabel();
        strLogo = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        cpr = new javax.swing.JLabel();
        perso = new javax.swing.JButton();
        b4 = new javax.swing.JButton();
        b9 = new javax.swing.JButton();
        b16 = new javax.swing.JButton();
        b25 = new javax.swing.JButton();
        credit = new javax.swing.JButton();
        stringsudoku = new javax.swing.JTextField();
        solve = new javax.swing.JButton();
        clear = new javax.swing.JButton();
        menu = new javax.swing.JButton();
        next = new javax.swing.JButton();
        back = new javax.swing.JButton();
        intro = new javax.swing.JLabel();
        back2 = new javax.swing.JButton();
        paper = new javax.swing.JLabel();
        infos = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sudoku Solver - INF452 ");
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocation(new java.awt.Point(100, 100));
        setLocationByPlatform(true);
        setMinimumSize(new java.awt.Dimension(800, 530));
        setResizable(false);
        setSize(new java.awt.Dimension(800, 530));
        getContentPane().setLayout(null);

        paneMain.setMinimumSize(new java.awt.Dimension(800, 500));
        paneMain.setPreferredSize(new java.awt.Dimension(800, 500));
        paneMain.setLayout(null);

        logomini.setIcon(new javax.swing.ImageIcon(getClass().getResource("/logomini.png"))); // NOI18N
        logomini.setMaximumSize(new java.awt.Dimension(220, 80));
        logomini.setMinimumSize(new java.awt.Dimension(210, 75));
        logomini.setPreferredSize(new java.awt.Dimension(210, 80));
        paneMain.add(logomini);
        logomini.setBounds(50, 20, 210, 80);

        strLogo.setFont(new java.awt.Font("Tw Cen MT Condensed", 3, 27)); // NOI18N
        strLogo.setForeground(new java.awt.Color(249, 200, 15));
        strLogo.setText("Solver");
        paneMain.add(strLogo);
        strLogo.setBounds(460, 100, 100, 40);

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/logo2.png"))); // NOI18N
        logo.setText("jLabel3");
        paneMain.add(logo);
        logo.setBounds(200, 10, 400, 140);

        cpr.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        cpr.setForeground(new java.awt.Color(249, 200, 15));
        cpr.setText("Copyright ©  2018 All rights reserved");
        paneMain.add(cpr);
        cpr.setBounds(280, 470, 240, 30);

        /*
        
        perso.setFont(new java.awt.Font("Arial Unicode MS", 1, 19)); // NOI18N
        perso.setText("Personalisé");
        perso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                persoActionPerformed(evt);
            }
        });
        paneMain.add(perso);
        perso.setBounds(310, 410, 180, 50);

        */
        
        b16.setFont(new java.awt.Font("Arial Unicode MS", 1, 19)); // NOI18N
        b16.setText(" 16 * 16 ");
        b16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b16ActionPerformed(evt);
            }
        });
        paneMain.add(b16);
        b16.setBounds(310, 290, 180, 50);

        b9.setFont(new java.awt.Font("Arial Unicode MS", 1, 19)); // NOI18N
        b9.setText(" 9 * 9 ");//intermediare  
        b9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b9ActionPerformed(evt);
            }
        });
        paneMain.add(b9);
        b9.setBounds(310, 230, 180, 50);

        b25.setFont(new java.awt.Font("Arial Unicode MS", 1, 19)); // NOI18N
        b25.setText(" 25 * 25 ");//Demoniaque  
        b25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b25ActionPerformed(evt);
            }
        });
        paneMain.add(b25);
        b25.setBounds(310, 350, 180, 50);

        b4.setFont(new java.awt.Font("Arial Unicode MS", 1, 19)); // NOI18N
        b4.setText(" 4 * 4 ");  //facile
        b4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b4ActionPerformed(evt);
            }
        });
        paneMain.add(b4);
        b4.setBounds(310, 170, 180, 50);

        credit.setFont(new java.awt.Font("Arial Unicode MS", 1, 19)); // NOI18N
        credit.setText("Credits");
        credit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                creditActionPerformed(evt);
            }
        });
        paneMain.add(credit);
        credit.setBounds(650, 410, 120, 50);

        stringsudoku.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        stringsudoku.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stringsudokuMouseClicked(evt);
            }
        });
        stringsudoku.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stringsudokuActionPerformed(evt);
            }
        });
        paneMain.add(stringsudoku);
        stringsudoku.setBounds(180, 210, 440, 40);

        solve.setFont(new java.awt.Font("Tw Cen MT", 1, 24)); // NOI18N
        solve.setText("Solve");
        solve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                solveActionPerformed(evt);
            }
        });
        paneMain.add(solve);
        solve.setBounds(80, 420, 140, 40);

        clear.setFont(new java.awt.Font("Tw Cen MT", 1, 24)); // NOI18N
        clear.setText("Clear");
        clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearActionPerformed(evt);
            }
        });
        paneMain.add(clear);
        clear.setBounds(80, 370, 140, 40);

        menu.setFont(new java.awt.Font("Tw Cen MT", 1, 24)); // NOI18N
        menu.setText("Menu");
        menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuActionPerformed(evt);
            }
        });
        paneMain.add(menu);
        menu.setBounds(80, 120, 140, 40);

        next.setFont(new java.awt.Font("Arial Unicode MS", 1, 18)); // NOI18N
        next.setText("Next");
        next.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextActionPerformed(evt);
            }
        });
        paneMain.add(next);
        next.setBounds(650, 210, 80, 40);

        back.setFont(new java.awt.Font("Arial Unicode MS", 1, 18)); // NOI18N
        back.setText("back");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        paneMain.add(back);
        back.setBounds(70, 210, 80, 40);

        intro.setFont(new java.awt.Font("Times New Roman", 3, 16)); // NOI18N
        intro.setForeground(new java.awt.Color(102, 102, 0));
        intro.setText("<html>Bienvenu dans notre Sudoku Solver,<br>ce programme a ete developpé par Chouaib Mounaime, Orand regis et Ayoub Ghammaz, etudiants en 2eme année de Licence Informatique a l'université Grenoble Alpes (UGA). <br>Ce projet a ete realisé afin de mettre en oeuvre les connaissances acquises en INF452 : logique propositionelle.  \n\n\n<br><br>Welcome to our Sudoku Solver,<br> this program was developped by Chouaib Mounaime, Orand regis et Ayoub Ghammaz, students of bachelor's second year at Grenoble Alpes University (UGA).<br> This project was commited to implement knowledges learned at INF452 : propositional logic. </html>");
        paneMain.add(intro);
        intro.setBounds(190, 110, 410, 266);
   
        
        
        
        infos.setBackground(new java.awt.Color(250,250,250));
        infos.setFont(new java.awt.Font("Tw Cen MT", 0, 22)); // NOI18N
        infos.setText("<html>•  Number of variables : NAN<br>•  Number of clauses : NAN<br>•  CPU time : NAN<br><br>•  Status : </html>");
        infos.setOpaque(true);
        paneMain.add(infos);
        infos.setBounds(30, 170, 260, 190);
        
        back2.setFont(new java.awt.Font("Tw Cen MT", 1, 23)); // NOI18N
        back2.setText("Back");
        back2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                back2ActionPerformed(evt);
            }
        });
        paneMain.add(back2);
        back2.setBounds(350, 430, 100, 30);

        paper.setFont(new java.awt.Font("Tw Cen MT", 1, 18)); // NOI18N
        paper.setForeground(new java.awt.Color(51, 51, 0));
        paper.setIcon(new javax.swing.ImageIcon(getClass().getResource("/paper.png"))); // NOI18N
        paneMain.add(paper);
        paper.setBounds(80, 40, 690, 410);

        //paneMain.add(paneGrid);
        
        background = new javax.swing.JLabel();
        
        background.setForeground(new java.awt.Color(130, 98, 13));
        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/purple-damask-full-hd-backgrounds-vintage-red-1642479 (1).jpg"))); // NOI18N
        paneMain.add(background);
        background.setBounds(0, 0, 910, 500);

        getContentPane().add(paneMain);
        paneMain.setBounds(0, 0, 800, 500);
        
        
        
        
        
        
        
        paneGrid   = new javax.swing.JPanel();
        grid = new javax.swing.JButton[9][9];
        
        paneGrid.setOpaque(false);
        paneGrid.setLayout(new java.awt.GridLayout(9,9));
        
        
        
        
        paneGrid4   = new javax.swing.JPanel();
        grid4 = new javax.swing.JButton[4][4];
        
        paneGrid4.setOpaque(false);
        paneGrid4.setLayout(new java.awt.GridLayout(4,4));
        
        
        
        paneGrid9   = new javax.swing.JPanel();
        grid9 = new javax.swing.JButton[9][9];
        
        paneGrid9.setOpaque(false);
        paneGrid9.setLayout(new java.awt.GridLayout(9, 9));
        
        
        
        paneGrid16   = new javax.swing.JPanel();
        grid16 = new javax.swing.JButton[16][16];
        
        paneGrid16.setOpaque(false);
        paneGrid16.setLayout(new java.awt.GridLayout(16,16));
        
        
        
        
        paneGrid25   = new javax.swing.JPanel();
        grid25 = new javax.swing.JButton[25][25];
        
        paneGrid25.setOpaque(false);
        paneGrid25.setLayout(new java.awt.GridLayout(25,25));
        
        for (int i=0; i<9 ;i++){
            for (int j=0; j<9 ;j++){
                grid[i][j] = new javax.swing.JButton();
                grid[i][j].setText("");
                paneGrid.add(grid[i][j]);
            }
        }       
        
        for (int i=0; i<4 ;i++){
            for (int j=0; j<4 ;j++){
                grid4[i][j] = new javax.swing.JButton();
                grid4[i][j].setText("");
                paneGrid4.add(grid4[i][j]);
            }
        }        
        
        for (int i=0; i<9 ;i++){
            for (int j=0; j<9 ;j++){
                grid9[i][j] = new javax.swing.JButton();
                grid9[i][j].setText("");
                paneGrid9.add(grid9[i][j]);
            }
        }        
        
        for (int i=0; i<16 ;i++){
            for (int j=0; j<16 ;j++){
                grid16[i][j] = new javax.swing.JButton();
                grid16[i][j].setText("");
                paneGrid16.add(grid16[i][j]);
            }
        }        
        
        for (int i=0; i<25 ;i++){
            for (int j=0; j<25;j++){
                grid25[i][j] = new javax.swing.JButton();
                grid25[i][j].setText("");
                paneGrid25.add(grid25[i][j]);
            }
        }         
        
        paneMain.add(paneGrid);
        paneGrid.setBounds(310, 10, 480, 480);
        
        paneMain.add(paneGrid4);
        paneGrid4.setBounds(310, 10, 480, 480);
        
        paneMain.add(paneGrid9);
        paneGrid9.setBounds(310, 10, 480, 480);
        
        paneMain.add(paneGrid16);
        paneGrid16.setBounds(310, 10, 480, 480);
        
        paneMain.add(paneGrid25);
        paneGrid25.setBounds(310, 10, 480, 480);
        
        pack();
    }// </editor-fold> 
     
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    /*private void initComponents() {
    
    }// </editor-fold>//GEN-END:initComponents
*/
    private void b4ActionPerformed(java.awt.event.ActionEvent evt) {           
        // TODO add your handling code here:
        niv=9;
        sdk = new Sudoku(4);
        System.out.println("Niveau 4*4 Random:");
        sdk.taille=4;
        sdk.initGridRandom(sdk.taille,niv,PERCENT);
        sdk.affichage_console(sdk.sudoku);
        /*
        paneGrid   = new javax.swing.JPanel();
        grid = new javax.swing.JButton[sdk.taille][sdk.taille];
        
        paneGrid.setOpaque(false);
        paneGrid.setLayout(new java.awt.GridLayout(sdk.taille, sdk.taille));
        
        for (int i=0; i<sdk.taille ;i++){
            for (int j=0; j<sdk.taille ;j++){
                grid[i][j] = new javax.swing.JButton();
                grid[i][j].setText("");
                paneGrid.add(grid[i][j]);
            }
        }        
        
        paneGrid.setBounds(310, 10, 480, 480);
        */
        gridColor4();
        remplirGrid4();
        
        paneGrid.setVisible(false);
        paneGrid4.setVisible(true);
        paneGrid9.setVisible(false);
        paneGrid16.setVisible(false);
        paneGrid25.setVisible(false);
        
        logomini.setVisible(true);
        solve.setVisible(true);
        menu.setVisible(true);
        clear.setVisible(true);
        strLogo.setVisible(false);
        b25.setVisible(false); 
        b4.setVisible(false); 
        b9.setVisible(false);
        b16.setVisible(false); 
        perso.setVisible(false); 
        logo.setVisible(false);
        credit.setVisible(false);
        cpr.setVisible(false);
        infos.setVisible(true);
    }                                  

    private void b9ActionPerformed(java.awt.event.ActionEvent evt) {                                   
        // TODO add your handling code here:
        
        niv=10;
        sdk = new Sudoku(9);
        System.out.println("Niveau 9*9 Random:");
        sdk.taille=9;
        sdk.initGridRandom(sdk.taille,niv,PERCENT);
      //p.initGridRandom(taille,niv,pourcentage de non affichage);
        sdk.affichage_console(sdk.sudoku);
        //sdk.initGridRandom(9,10,80);
        //initGridRandom(taille,niv,pourcentage de non affichage);
        //interm  diab  dem
        
        
        
        gridColor9();
        remplirGrid9();
        
        paneGrid.setVisible(false);
        paneGrid4.setVisible(false);
        paneGrid9.setVisible(true);
        paneGrid16.setVisible(false);
        paneGrid25.setVisible(false);
        
        logomini.setVisible(true);
        solve.setVisible(true);
        menu.setVisible(true);
        clear.setVisible(true);
        strLogo.setVisible(false);
        b25.setVisible(false); 
        b4.setVisible(false); 
        b9.setVisible(false);
        b16.setVisible(false); 
        perso.setVisible(false); 
        logo.setVisible(false);
        credit.setVisible(false);
        cpr.setVisible(false);
        infos.setVisible(true);
    }                                     
                                 

    private void b25ActionPerformed(java.awt.event.ActionEvent evt) {   
        // TODO add your handling code here:
        
        niv=12;
        sdk = new Sudoku(25);
        System.out.println("Niveau 25*25 Random:");
        sdk.taille=25;
        sdk.initGridRandom(sdk.taille,niv,PERCENT);
      //p.initGridRandom(taille,niv,pourcentage de non affichage);
        sdk.affichage_console(sdk.sudoku);
        //sdk.initGridRandom(9,10,80);
        //initGridRandom(taille,niv,pourcentage de non affichage);
        //interm  diab  dem
        
        
        gridColor25();
        remplirGrid25();
        
        paneGrid.setVisible(false);
        paneGrid4.setVisible(false);
        paneGrid9.setVisible(false);
        paneGrid16.setVisible(false);
        paneGrid25.setVisible(true);
        
        logomini.setVisible(true);
        solve.setVisible(true);
        menu.setVisible(true);
        clear.setVisible(true);
        strLogo.setVisible(false);
        b25.setVisible(false); 
        b4.setVisible(false); 
        b9.setVisible(false);
        b16.setVisible(false); 
        perso.setVisible(false); 
        logo.setVisible(false);
        credit.setVisible(false);
        cpr.setVisible(false);
        infos.setVisible(true);
    }                                   

    private void b16ActionPerformed(java.awt.event.ActionEvent evt) {         
        // TODO add your handling code here:
        
        niv=11;
        sdk = new Sudoku(16);
        System.out.println("Niveau 16*16 Random:");
        sdk.taille=16;
        sdk.initGridRandom(sdk.taille,niv,PERCENT);
      //p.initGridRandom(taille,niv,pourcentage de non affichage);
        sdk.affichage_console(sdk.sudoku);
        //sdk.initGridRandom(9,10,80);
        //initGridRandom(taille,niv,pourcentage de non affichage);
        //interm  diab  dem
        
        
        gridColor16();
        remplirGrid16();
        
        paneGrid.setVisible(false);
        paneGrid4.setVisible(false);
        paneGrid9.setVisible(false);
        paneGrid16.setVisible(true);
        paneGrid25.setVisible(false);
        
        
        logomini.setVisible(true);
        solve.setVisible(true);
        menu.setVisible(true);
        clear.setVisible(true);
        strLogo.setVisible(false);
        b25.setVisible(false); 
        b4.setVisible(false); 
        b9.setVisible(false);
        b16.setVisible(false); 
        perso.setVisible(false); 
        logo.setVisible(false);
        credit.setVisible(false);
        cpr.setVisible(false);
        infos.setVisible(true);
    }                                          

        

    private void persoActionPerformed(java.awt.event.ActionEvent evt) {   
        b25.setVisible(false); 
        b4.setVisible(false); 
        b9.setVisible(false);
        b16.setVisible(false); 
        paneGrid.setVisible(false);
        paneGrid4.setVisible(false);
        paneGrid9.setVisible(true);
        paneGrid16.setVisible(false);
        paneGrid25.setVisible(false);
        niv=8;
        sdk = new Sudoku(9);
        sdk.taille=9;
        sdk.initGridRandom(sdk.taille,niv,80);
        //initGridRandom(taille,niv,pourcentage de non affichage);
        //diab  dem
        try{
        sdk.MAJGrid("./src/Niveau_sudoku/b9_sudoku.txt");
        }catch (IOException io) { 
            throw new RuntimeException("Erreur: persoActionPerf() "+io);
        }    
        remplirGrid9();
    }                               

    private void stringsudokuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stringsudokuActionPerformed
        // TODO add your handling code here:
        strperso = ((JTextField)evt.getSource()).getText();
    }//GEN-LAST:event_stringsudokuActionPerformed

    private void stringsudokuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stringsudokuMouseClicked
        // TODO add your handling code here:
        JTextField jtf = ((JTextField)evt.getSource());
        jtf.setText("");
        jtf.getFont().deriveFont(Font.PLAIN);
        jtf.setForeground(Color.black);
        //jtf.removeMouseListener((MouseListener) this);
    }//GEN-LAST:event_stringsudokuMouseClicked

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        System.out.println("back : ");
        b4.setVisible(true); 
        b9.setVisible(true);
        b25.setVisible(true); 
        b16.setVisible(true);
        perso.setVisible(true); 
        credit.setVisible(true);
        stringsudoku.setVisible(false); 
        back.setVisible(false);
        next.setVisible(false);
        infos.setVisible(false);
    }//GEN-LAST:event_backActionPerformed

    private void menuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuActionPerformed
        // TODO add your handling code here:
        System.out.println("Menu : ");
        b4.setVisible(true); 
        b9.setVisible(true);
        b25.setVisible(true); 
        b16.setVisible(true);
        
        //perso.setVisible(true);
        perso.setVisible(false);
        
        
        logo.setVisible(true);
        strLogo.setVisible(true);
        solve.setVisible(false);
        menu.setVisible(false);
        clear.setVisible(false);
        logomini.setVisible(false);
        credit.setVisible(true);
        cpr.setVisible(true);
        paneGrid.setVisible(false);
        paneGrid4.setVisible(false);
        paneGrid9.setVisible(false);
        paneGrid16.setVisible(false);
        paneGrid25.setVisible(false);
        infos.setVisible(false);
    }//GEN-LAST:event_menuActionPerformed

    private void nextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextActionPerformed
        // TODO add your handling code here:
        
        System.out.println("Next : ");
        sdk = new Sudoku("./src/Niveau_sudoku/b9_sudoku.txt",9);
        sdk.Manuelle9Panel(strperso);
        remplirGrid9();
        sdk.affichage_console(sdk.sudoku);
        
        solve.setVisible(true);
        menu.setVisible(true);
        clear.setVisible(true);
        logomini.setVisible(true);
        infos.setVisible(true);
        back.setVisible(false);
        next.setVisible(false);
        stringsudoku.setVisible(false);
        logo.setVisible(false);
        strLogo.setVisible(false);
        cpr.setVisible(false);
        paneGrid.setVisible(false);
        paneGrid4.setVisible(false);
        paneGrid9.setVisible(false);
        paneGrid16.setVisible(false);
        paneGrid25.setVisible(false);
    }//GEN-LAST:event_nextActionPerformed

    private void back2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_back2ActionPerformed
        // TODO add your handling code here:
        System.out.println("back2 : ");
        b4.setVisible(true); 
        b9.setVisible(true);
        b25.setVisible(true); 
        b16.setVisible(true);
        //25->diab 16->intermediare 9->Demoniaque 4->facile
        perso.setVisible(true); 
        logo.setVisible(true);
        strLogo.setVisible(true);
        credit.setVisible(true);
        intro.setVisible(false);
        back2.setVisible(false);
        paper.setVisible(false);
        cpr.setVisible(true);
        paneGrid.setVisible(false);
        paneGrid4.setVisible(false);
        paneGrid9.setVisible(false);
        paneGrid16.setVisible(false);
        paneGrid25.setVisible(false);
        infos.setVisible(false);
    }//GEN-LAST:event_back2ActionPerformed

    private void creditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_creditActionPerformed
        // TODO add your handling code here:
        
        System.out.println("credit : ");
        b25.setVisible(false); 
        b4.setVisible(false); 
        b9.setVisible(false);
        b16.setVisible(false); 
        perso.setVisible(false); 
        credit.setVisible(false);
        paper.setVisible(true);
        back2.setVisible(true);
        intro.setVisible(true);
        logo.setVisible(false);
        strLogo.setVisible(false);
        infos.setVisible(false);
    }//GEN-LAST:event_creditActionPerformed

    private void clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearActionPerformed
        // TODO add your handling code here:
        System.out.println("clear : ");
        if (niv == 8){
            sdk = new Sudoku("./src/Niveau_sudoku/b9_sudoku.txt",9);
            sdk.Manuelle9Panel(strperso);
        }else if (niv == 9){
            remplirGrid4();
        }else if (niv == 10){
            remplirGrid9();
        }else if (niv == 11){
            remplirGrid16();
        }else if (niv == 12){
            remplirGrid25();    
        }
        sdk.affichage_console(sdk.sudoku);
    }//GEN-LAST:event_clearActionPerformed

    private void solveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_solveActionPerformed
        // TODO add your handling code here:
        try{
            sdk.MAJGrid("./src/Niveau_sudoku/b"+sdk.taille+"_sudoku.txt");
            //sdk.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",sdk.sudoku,niv,"p cnf 729 14000");
            sdk.solution(sdk.sudoku,"./src/SAT/SOL"+niv+"_Sudoku.sat",niv);
        }catch(FileNotFoundException e){}
        
        sdk.recuperePhraseSAT(niv);
        infos.setText("<html>•  Number of variables : "+sdk.Phrase[1]+"<br>•  Number of clauses : "+sdk.Phrase[2]+"<br>•  CPU time : "+sdk.Phrase[10]+" s<br><br>•  Status : "+sdk.Phrase[11]+"</html>");
        
        sdk.affichage_console(sdk.sudoku);
        //Gridsol();
        if(niv==9){
        Gridsol4();
        sdk = new Sudoku("./src/Niveau_sudoku/b4_sudoku.txt",4);
        remplirGrid4();
        }
        else if(niv==10){
        Gridsol9();
        sdk = new Sudoku("./src/Niveau_sudoku/b9_sudoku.txt",9);
         remplirGrid9();
        }
        else if(niv==11){
        Gridsol16();
        sdk = new Sudoku("./src/Niveau_sudoku/b16_sudoku.txt",16);
         remplirGrid16();
        }
        else if(niv==12){
        Gridsol25();
        sdk = new Sudoku("./src/Niveau_sudoku/b25_sudoku.txt",25);
         remplirGrid25();
        }
        
        //remplirGrid();
    }//GEN-LAST:event_solveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                NewJFrame jf = new NewJFrame();
                jf.setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JButton back2;
    private javax.swing.JLabel background;
    private javax.swing.JButton clear;
    private javax.swing.JLabel cpr;
    private javax.swing.JButton credit;
    private javax.swing.JButton b25;
    private javax.swing.JButton b16;
    private javax.swing.JButton b4;
    private javax.swing.JLabel infos;
    private javax.swing.JButton b9;
    private javax.swing.JLabel intro;
    private javax.swing.JLabel logo;
    private javax.swing.JLabel logomini;
    private javax.swing.JButton menu;
    private javax.swing.JButton next;
    //private javax.swing.JPanel paneGrid;
    private javax.swing.JPanel paneMain;
    private javax.swing.JLabel paper;
    private javax.swing.JButton perso;
    private javax.swing.JButton solve;
    private javax.swing.JLabel strLogo;
    private javax.swing.JTextField stringsudoku;
    // End of variables declaration//GEN-END:variables
}

