//import csjflib.Entree;
import java.lang.Math;
import java.io.*;
import java.util.*;
import java.util.Scanner;
import java.lang.System.*;
import java.lang.Runtime;
import java.util.Random;
/*   *
     * @author ORAND Regis - Mounaim Chouaib - Ghammaz Ayoub  L2 INF2
     * @version Version 07/02/18
*/
public class Sudoku{

    //Attribut
    public static final int INITIAL_VALUE = 0;
    //public static List randomList = new ArrayList();
        
    public String[] Phrase = new String[16];
    public int[][] sudoku;//Un sudoku de dimension 2
    public int taille_MAX = 9;//taille du sudoku
    public int taille=9;
   
    public String nomFic_default = "./src/Niveau_sudoku/facile_sudoku.txt";
    public String mot; // texte parcouru.
    public String nomFic;
    public int cc;  // caractere courant en ASCII
    public InputStream ips; 
    public InputStreamReader isr; 
    
    //Constructeur par defaut
    //Parcours un fichier et recupere chaque entiers pour les mettres
    //dans le sudoku initialise au prealable
    public  Sudoku(String nom, int Taille){
        int i=0,j=0;
        this.taille = Taille;
        sudoku = new int[taille+1][taille+1];
        this.mot ="";
    	this.nomFic = nom;
        demarrer(Taille);
        while(!finDeSequence())
        {
            for(i=0;i<taille;i++){
             for(j=0;j<taille;j++){
                 
                 mot=elementCourant(); 
                 sudoku[i][j] = Integer.parseInt(mot);  
                 avancer();
              }
            } 
        }
    }
    
    public  Sudoku(int taille_MAX ){
        int i=0,j=0;
        this.taille = taille_MAX;
        sudoku = new int[taille_MAX+1][taille_MAX+1];
            for(i=0;i<taille;i++){
             for(j=0;j<taille;j++){
                 sudoku[i][j] = 0;
             } 
            }
    }
    
    public void initGridRandom(int Taille,int niv,int percent) {
        try{
            
        List randomList = new ArrayList();    
        for (int i=0; i<taille ;i++){
            for (int j=0; j<taille ;j++){
                sudoku[i][j]=0;
            }
        }
        int rand=0;
        Random randomGenerator = new Random((new Date().getTime()));
        int lig = randomGenerator.nextInt(Taille)+1; 
        for( int k = 0 ; k <Taille; k++) {
            randomList.clear();
            do {
                rand = randomGenerator.nextInt(Taille)+1;   // generate a random number
                if( randomList.contains(rand ) ) {   // already tested
                    continue;
                } else {
                    randomList.add(rand);			// add to alreadyDonelist
                    if(randomList.size() == Taille) {	// cancel if all tested
                        rand = 0;
                        break;
                    }
                }

            } while ( !checkRow(rand,lig, k) ) ;
            sudoku[lig][k]=rand;  
        }
        
        sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",sudoku,niv,"p cnf 729 14000");
        Salut_Systeme.exec("./src/minisat/core/minisat ./src/CNF/NIV"+niv+"_Sudoku.cnf ./src/SAT/SOL"+niv+"_Sudoku.sat; exit $?");
        
        solution(sudoku,"./src/SAT/SOL"+niv+"_Sudoku.sat",niv);
        remplirGridSudoku(percent);
        
        }catch (IOException io) { 
            throw new RuntimeException("Erreur: initGridRandom() "+io);
        } 
    }
    
    public void remplirGridSudoku(int POURCENT_CACHE ) {
		Random randomGenerator = new Random((new Date().getTime()));
		for( int k = 0 ; k < taille ; k++) {
			for( int j = 0 ; j < taille ; j++) {			  // display the game
                            if( randomGenerator.nextInt(100) < POURCENT_CACHE) {  // percent hidden number
                                    sudoku[k][j]=0;
                            }
			}		
		}
	}
    
    
    
    private boolean checkRow( int randomNumber, int row, int col) {
        //  cols avant
        for( int i=0; i < col ; i++) {
                if( randomNumber == sudoku[row][i] )  
                        return false;
        }
        // cols apres, dans start
        for( int i=col; i < taille  ; i++) {
                if( randomNumber == sudoku[row][i] )  
                        return false;
        }
        return true;
    }
    
 
 public void recuperePhraseSAT(int niveau){
     Salut_Systeme.exec("./src/minisat/core/minisat ./src/CNF/NIV"+niveau+"_Sudoku.cnf ./src/SAT/SOL"+niveau+"_Sudoku.sat > RECUPERATION.txt; exit $?");    
        this.mot ="";
    	this.nomFic = "RECUPERATION.txt";
        demarrer3(9);
        int i=1;
        avancer3();
        while(!finDeSequence())
        {
                 if(compare2chaine(elementCourant(),":") ){
                     avancer3();
                     Phrase[i]=elementCourant(); 
                     i++;
                 }
                 avancer3();
        }
        Phrase[11]="SATISFIABLE          ";
 }
    
 
 public void setSudoku(int[][] sudoku,int x,int y,int val){
     System.out.println("Entrer un chiffre compris entre 1 et 9 :");
     /*Scanner sc = new Scanner(System.in);
     int val = sc.nextInt();
     */if(x>0 && x<10 && y>0 && y<10 && val>0 && val<10 && sudoku[x][y]==0){
             sudoku[x][y]=val;
     }
 } 
 
    public String getNomFic(){
	    return this.nomFic;
	}
    
    public void setNomFic(String Fich){
	    this.nomFic = Fich;
	}
    
    
    public int getTaille(){
	    return this.taille;
	}
    
    public void setTaille(int Taille){
	    this.taille = Taille;
	}
    
    //Permet de arcourir un fichier ouvert en lecture
    public void demarrer(int Taille){
         try{
              this.ips=new FileInputStream(this.nomFic);
	          this.isr = new InputStreamReader(this.ips);
	     	  this.mot ="";
	     	  this.cc =' ';
	     	  avancer();
         }catch (IOException io) {
                throw new RuntimeException("Erreur: demmarer() "+io);}
    }    
    
    //Permet de arcourir un fichier ouvert en lecture
    public void demarrer3(int Taille){
         try{
              this.ips=new FileInputStream(this.nomFic);
	          this.isr = new InputStreamReader(this.ips);
	     	  this.mot ="";
	     	  this.cc =' ';
	     	  avancer3();
         }catch (IOException io) {
                throw new RuntimeException("Erreur: demmarer() "+io);}
    } 
    
    //avancer selon les separateurs choisis et place l'elementCourant dans mot
    //Separateur : (c=='|')||(c=='-')||(c==' ')||(c=='\n')
    public void avancer(){  
        this.mot = "";
        try{
            if(!finDeSequence()){
		         this.cc = this.isr.read();
		         while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();}
		         while( !finDeSequence() &&  !estSep((char)cc)){
			 	 	this.mot = this.mot+(char)this.cc;
			 	 	this.cc = this.isr.read();}
		      }
		  }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer() "+io);}
    } 
    
    //avancer selon les separateurs choisis et place l'elementCourant dans mot
    //Separateur : (c=='|')||(c==' ')||(c=='\n');
    public void avancer2(){  
        this.mot = "";
        try{
            if(!finDeSequence()){
		         this.cc = this.isr.read();
		         while( !finDeSequence() && estSep2((char)cc) ){
		     	      this.cc = this.isr.read();}
		         while( !finDeSequence() &&  !estSep2((char)cc)){
			 	 	this.mot = this.mot+(char)this.cc;
			 	 	this.cc = this.isr.read();}
		      }
		  }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer2() "+io);}
    }
    
    public void avancer3(){  
        this.mot = "";
        try{
            if(!finDeSequence()){
		         this.cc = this.isr.read();
		         while( !finDeSequence() && estSep3((char)cc) ){
		     	      this.cc = this.isr.read();}
		         while( !finDeSequence() &&  !estSep3((char)cc)){
			 	 	this.mot = this.mot+(char)this.cc;
			 	 	this.cc = this.isr.read();}
		      }
		  }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer3() "+io);}
    }
    
    //Permet de savoir si on est a la fin fu fichier
    public boolean finDeSequence(){
         try{   
            if (this.cc == -1) this.isr.close();
            return (this.cc == -1); 
         }catch (IOException io) {
            throw new RuntimeException("Erreur: finDeSequence() "+io);}
     }

    //L'elementCourant est le mot courant
    public String elementCourant(){
     		return this.mot;
    }
    
    //Separateur
    public boolean estSep(char c){
        return (c=='|')||(c=='-')||(c==' ')||(c=='\n');
     }     
     
    //Separateur 
    public boolean estSep2(char c){
        return (c=='|')||(c==' ')||(c=='\n');
     } 
    
    //Separateur 
    public boolean estSep3(char c){
        return (c!='0')&&(c!='1')&&(c!='2')&&(c!='3')&&(c!='4')&&(c!='5')&&(c!='6')&&(c!='7')&&(c!='8')&&(c!='9')&&(c!='.')&&(c!=':');
     } 
     
     
     
     
     
     
     
     

   
   //Permet d'afficher sur la console le sudoku 9*9
   public void affichage_console(int[][] sudoku){
    int i =0,j=0;
    int res = (int)Math.sqrt((double)taille);
    while (i != taille) {
       if(i%res==0){ System.out.println(""); }
       while (j != taille) {
             if(j%res==0){ System.out.print("|");}
             if(sudoku[i][j]<=taille  && sudoku[i][j]<10){
                    System.out.print(sudoku[i][j]+"  ");}
             else{
                    System.out.print(sudoku[i][j]+" ");}
             j++;
       }
       System.out.println("|");
       j=0;
       i++;
    }     
    
    System.out.println("");
   }
   
  //Permet d'afficher sur le fichier dimacs le sudoku 4*4
  public void affichage_dimacs_sudoku_4(int[][] sudoku,String prob,PrintWriter pw){
        int i=0,j=0;
        int res = (int)Math.sqrt((double)taille);
        while (i != taille) {
           if(i%res==0){ pw.println("");}
           while (j != taille) {
                 if(j==0){ pw.print("c|");}
                 else if(j%res==0){ pw.print("|");}
                 if(sudoku[i][j]<=taille){
                        pw.print(sudoku[i][j]+"  ");}
                 j++;
           }
           pw.println("|");
           j=0;
           i++;
        }     
        pw.println("");
        pw.println(" ");
        pw.println(" ");
        pw.println(" ");
        pw.println(prob);
  }

  //Permet d'afficher sur le fichier dimacs le sudoku 9*9
  public void affichage_dimacs_sudoku(int[][] sudoku,String prob,PrintWriter pw){
        int lig=0,col=0;
        int res = (int)Math.sqrt((double)taille);
        while (lig != taille) {
            if(lig%res==0){ pw.println("");}
            while (col != taille){
                        if(col==0){ pw.print("c|");}
                        else if(col%res==0){ pw.print("|");}
                        if(sudoku[lig][col]<=taille  && sudoku[lig][col]<10){
                               pw.print(sudoku[lig][col]+"  ");}
                        else{
                               pw.print(sudoku[lig][col]+" ");}
                        col++;
       	    }
       		pw.println("|");
       		col=0;
       		lig++;
        }
        pw.println("");
        pw.println(" ");
        pw.println(" ");
        pw.println(" ");
        pw.println(prob);
  }
  
  //Permet d'afficher sur le fichier dimacs les litteraux du sudoku
  //Soit, les valeurs definie par defaut, a l'initialisation
  public void affichage_dimacs_litteraux(int[][] sudoku,PrintWriter pw){
        int lig=0,col=0;
        while (lig != taille) {
            while (col != taille){
                         if(0<sudoku[lig][col] && sudoku[lig][col]<=taille){
                                pw.println(codage(lig,col,sudoku[lig][col])+" 0");}
                         col++;
       	    }
       		col=0;
       		lig++;
        }
  }
  
  //Permet d'afficher sur le fichier dimacs les clausse generer pour 
  //Qu'un chiffre n'apparaissent qu'une et une seule fois par case
  public void affichage_dimacs_1chiffre_par_case(int[][] sudoku,PrintWriter pw){
        int val=1,lig=0,col=0;
        for(lig=0;lig<taille;lig++){
            for(col=0;col<taille;col++){
                for(val=1;val<=taille;val++){
                    pw.print(codage(lig,col,val)+" ");
                }
                pw.println(" 0");
            }
        }
  }
  
  //Permet d'afficher sur le fichier dimacs les clausse generer pour 
  //Qu'un chiffre n'apparaissent qu'une et une seule fois par ligne
  public void affichage_dimacs_1chiffre_par_ligne(int[][] sudoku,PrintWriter pw){
        int lig=0,col=0;
        for(lig=0;lig<taille;lig++){
            for(col=0;col<taille;col++){
            
               for(int val=1;val<=taille;val++){
                    for(int col2=0;col2<taille;col2++){
                        if(col!=col2) pw.println("-"+codage(lig,col,val)+" -"+codage(lig,col2,val)+" 0");
                   }
               }
            }
       }
  }

  //Permet d'afficher sur le fichier dimacs les clausse generer pour 
  //Qu'un chiffre n'apparaissent qu'une et une seule fois par colonne
  public void affichage_dimacs_1chiffre_par_colonne(int[][] sudoku,PrintWriter pw){
        int lig=0,col=0;
        for(lig=0;lig<taille;lig++){
            for(col=0;col<taille;col++){
            
               //val n'apparait pas 2 fois sur une colonne
               for(int val=1;val<=taille;val++){
                    for(int lig2=0;lig2<taille;lig2++){
                        if(lig!=lig2) pw.println("-"+codage(lig,col,val)+" -"+codage(lig2,col,val)+" 0"); 
                     }
               }
            }
       }
  }

  //Permet d'afficher sur le fichier dimacs les clausse generer pour 
  //Qu'un chiffre n'apparaissent qu'une et une seule fois par carre dans un sudoku 9*9
  public void affichage_dimacs_1chiffre_par_carre_4(int[][] sudoku,PrintWriter pw){
        int lig=0,col=0;
        int res = (int)Math.sqrt((double)taille);
        for(lig=0;lig<taille;lig++){
            for(col=0;col<taille;col++){
            
                    //val n'apparait pas 2 fois sur un carre
                   for(int val=1;val<=taille;val++){
                       for(int i=(lig/res)*res;i<(lig/res)*res+res;i++){
                         for(int j=(col/res)*res;j<(col/res)*res+res;j++){
                            if(lig!=i || col!=j){
                               pw.println("-"+codage(lig,col,val)+" -"+codage(i,j,val)+" 0"); }
                         }
                       }
                   }
            }
       }
  }
  


  //Permet d'afficher sur le fichier dimacs les clausse generer pour 
  //Qu'un chiffre n'apparaissent qu'une et une seule fois par carre dans un sudoku 4*4
   public void affichage_dimacs_1chiffre_par_carre_9(int[][] sudoku,PrintWriter pw){
        int lig=0,col=0;
        int res = (int)Math.sqrt((double)taille);
        for(lig=0;lig<taille;lig++){
            for(col=0;col<taille;col++){
            
                   //val n'apparait pas 2 fois sur un carre
                   for(int val=1;val<=taille;val++){
                       for(int i=(lig/res)*res;i<(lig/res)*res+res;i++){
                         for(int j=(col/res)*res;j<(col/res)*res+res;j++){
                            if(lig!=i || col!=j) {
                                pw.println("-"+codage(lig,col,val)+" -"+codage(i,j,val)+" 0");  }
                         }
                       }
                   }
            }
       }
  }

   //Permet de sauver dans un fichier DIMACS le sudoku choisi grace au clause et litteraux generer
   public void sauver(String nomDeFichier,int[][] sudoku, int niv,String prob) throws FileNotFoundException{	  
    	OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(nomDeFichier));
        BufferedWriter bw = new BufferedWriter(writer);
        PrintWriter pw = new PrintWriter(bw);
        
        if(niv==1){
           pw.println("c Niveau facile:");}
        else if(niv==2){
           pw.println("c Niveau moyen:");}
        else if(niv==3){
           pw.println("c Niveau difficile:");}
        else if(niv==4){
           pw.println("c Niveau diabolique:");} 
        else if(niv==5){
           pw.println("c Niveau demoniaque:");} 
        else if(niv==6){
           pw.println("c Niveau 4*4:");} 
        else if(niv==7){
           pw.println("c Manuellement 4*4:");}
        else if(niv==8){
           pw.println("c Manuellement 9*9:");}
        else if(niv==9){
           pw.println("c Niveau 4*4 random:");}
        else if(niv==10){
           pw.println("c Niveau 9*9 random:");}
        else if(niv==11){
           pw.println("c Niveau 16*16 random:");}
        else if(niv==12){
           pw.println("c Niveau 25*25 random:");}
        else{
           pw.println("c Niveau par defaut -> facile:");}
        
        if(niv==6){  affichage_dimacs_sudoku_4(sudoku,prob,pw); }
        else{        affichage_dimacs_sudoku(sudoku,prob,pw); }
        
        affichage_dimacs_litteraux(sudoku,pw);
        
        affichage_dimacs_1chiffre_par_case(sudoku,pw);
        
        affichage_dimacs_1chiffre_par_ligne(sudoku,pw);
        
        affichage_dimacs_1chiffre_par_colonne(sudoku,pw);
              
        if(niv==6) affichage_dimacs_1chiffre_par_carre_4(sudoku,pw);
        else       affichage_dimacs_1chiffre_par_carre_9(sudoku,pw);
        
        pw.println("");
        pw.close() ;
   }
   public void MAJGrid(String nomDeFichier) throws FileNotFoundException{	  
    	OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(nomDeFichier));
        BufferedWriter bw = new BufferedWriter(writer);
        PrintWriter pw = new PrintWriter(bw);
        
        for(int i=0;i<taille;i++){
            for(int j=0;j<taille;j++){
                 pw.print(sudoku[i][j]+" ");
            }
             pw.println("");
        }
        pw.println("");
        pw.close() ;
   }
   /**
     * renvoie true ssi s1 et egale a s2;
     * @param s1 et s2 sont des chaine de charactere
     * @return true or false
     */
    public boolean compare2chaine(String s1,String s2){
        int i=0;
        int j=0;
        
            while( j!=s1.length()  && !(s1.isEmpty())  &&  i!=s2.length()  && !(s2.isEmpty())   ) {
               if(java.lang.Character.toLowerCase(s2.charAt(i))== java.lang.Character.toLowerCase(s1.charAt(j)) ){
                  j++;
                  i++;
               }
               else   return false;
           } 
           
           return ( j==s1.length() && i==s2.length()  )  ;
    }
   
   
    
    //Lecture d'un fichier dimacs pour recuperer la solution du sudoku
    public void solution(int[][] sudoku,String nom,int niv){
        
            System.out.println("----------------           SOLUTION            ----------------");
            System.out.println("");
            
            //Salut_Systeme fait partie d'une autre classe qui me permet d'execute une commande shell depuis se fichier
            Salut_Systeme.exec("./src/minisat/core/minisat ./src/CNF/NIV"+niv+"_Sudoku.cnf ./src/SAT/SOL"+niv+"_Sudoku.sat; exit $?");
            
            System.out.println("");
            int i=0;
            int chiffre=0;
            int[]  tab = new int[3];
            this.mot ="";
            this.nomFic = nom;
            if(niv==6) demarrer(4);
            else demarrer(9);
            if(compare2chaine("SAT",elementCourant()) ){
                avancer2();//Pour ne pas prendre le SAT ou UNSAT sur la premiere ligne
                while(!finDeSequence())
                {
                    mot=elementCourant(); 
                    chiffre=Integer.parseInt(mot);  
                    if(chiffre>0){
                        decodage(chiffre,tab);
                        sudoku[tab[0]][tab[1]] = tab[2];
                    }
                    avancer2();
                }
            }else{
                        //si le fichier n'a pas de solution
                       System.out.println("----------------         PROBLEME              ----------------");
                       System.out.println("Le fichier solution ./src/SAT/SOL"+niv+"_Sudoku.sat "); 
                       System.out.println("Indique qu'il n'y a pas de solution au probleme pose");
            }
            System.out.println(" Fin de la solution");
       
    }
    
    //Codage grace a la ligne colonne et valeur donne en parametre
    public int codage(int ligne,int colonne,int val){
        return (((ligne*taille)+colonne)*taille+val);
    }
    
    //Decodage d'un entier
     //Il n'est pas possible de renvoye 3 valeurs en meme temps donc nous ne mettons dans un tableau
    public void decodage(int N,int[] tab){
       /* ligne   */         tab[0]= ( (N-1) / (taille*taille) );
       /* colonne */         tab[1]= ( (N-1) % (taille*taille) ) / taille;
       /* valeur  */         tab[2]= ( N - (( tab[0]*taille)+tab[1])*taille );
            
    }
    
    //Permet d'afficher un MENU sur la console avec un choix a faire
    //Affichage du sudoku sur la console
    //Sauvegarde du fichier DIMACS et renvoie le niveau de difficulte choisi
    public int Menu(){
               Scanner sc = new Scanner(System.in);
               System.out.println("---------------------------------------------------------------");
               System.out.println("----------------              SUDOKU           ----------------");
               System.out.println("---------------------------------------------------------------");
               System.out.println(" FAITES VOTRE CHOIX :");
               System.out.println("---------------------------------------------------------------");
               System.out.println("- 1 ) Niveau facile:");
               System.out.println("- 2 ) Niveau moyen:");
               System.out.println("- 3 ) Niveau difficile:");
               System.out.println("- 4 ) Niveau diabolique:");
               System.out.println("- 5 ) Niveau demoniaque:");
               System.out.println("- 6 ) Niveau 4*4:");
               System.out.println("- 7 ) Manuellement 4*4:");
               System.out.println("- 8 ) Manuellement 9*9:");
               System.out.println("- 9 ) Niveau 4*4 random:");
               System.out.println("- 10) Niveau 9*9 random:");
               System.out.println("- 11) Niveau 16*16 random:");
               System.out.println("- 12) Niveau 25*25 random:");
               System.out.println("---------------------");
               System.out.println("---------------------");
               System.out.print("Choix :");
               taille = 16;
               return sc.nextInt();
    }
    
    //Permet de recuperer manuellement un sudoku depuis le terminal grace a l utilisateur
    //Pour un sudoku 4*4
    public void Manuelle4(int[][] sudoku){
        int lig=0,col=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Les autres valeurs seront mis a zero ");
        System.out.println("De gauche a droite et de bas en haut");
        System.out.println("Exemple ");
        System.out.println("3410120003210043");
        System.out.println("Entrer le sudoku:");
        String val = sc.nextLine();
        int i=0;
        int n = val.length();  // il faut rajouter 1
        int[] tab = new int[n];
        
        for(i=n-1;0<=i;i--)  tab[i] = (val.charAt(i)-'0') ;
        i=0;
        for(lig=0;lig<4;lig++){
            for(col=0;col<4;col++){
                if(i<=n-1) sudoku[lig][col]=tab[i];
                else       sudoku[lig][col]=0;
                i++;
            }
        }
    }
    
    //Permet de recuperer manuellement un sudoku depuis le terminal grace a l utilisateur
    //Pour un sudoku 9*9
    public void Manuelle9(int[][] sudoku){
        int lig=0,col=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Les autres valeurs seront mis a zero ");
        System.out.println("De gauche a droite ");
        System.out.println("Exemple ");
        System.out.println("200075419004136500018332367000649027500046793117000684001653792967214800325790006");
        System.out.println("Entrer le sudoku:");
        String val = sc.nextLine();
        int i=0;
        int n = val.length();  // il faut rajouter 1
        int[] tab = new int[n];
        
        for(i=n-1;0<=i;i--)  tab[i] = (val.charAt(i)-'0') ;
        i=0;
        for(lig=0;lig<9;lig++){
            for(col=0;col<9;col++){
                if(i<=n) sudoku[lig][col]=tab[i];
                else     sudoku[lig][col]=0;
                i++;
            }
        }
        
    }
   
    public void Manuelle9Panel(String val){
        int lig=0,col=0;
        int i=0;
        int n = val.length();  // il faut rajouter 1
        int[] tab = new int[n];
        System.out.println("n->"+n);
        int res = (int)Math.sqrt((double)taille);
        //((Long) arg2).intValue();
        for(i=n-1;0<=i;i--)  tab[i] = (val.charAt(i)-'0') ;
        i=0;
        for(lig=0;lig<taille;lig++){
            for(col=0;col<taille;col++){
                if(i<=n-1) sudoku[lig][col]=tab[i];
                else       sudoku[lig][col]=0;
                i++;
            }
        }
    }
    
    
    public static void main(String[] args) throws FileNotFoundException{
       try{
       
           int[]  tab = new int[3];
           int niv = 0,nb_case=9,cpt=0;
           Scanner reader = new Scanner(System.in);
           
           Sudoku p = new Sudoku("./src/Niveau_sudoku/facile_sudoku.txt",nb_case); 
           String rejouer="o";
           while(rejouer.charAt(0)=='o' || rejouer.charAt(0)=='O'  || rejouer.charAt(0)=='0'){
           
                   //Permet d'afficher un MENU sur la console avec un choix a faire
                   //Affichage du sudoku sur la console
                   //Sauvegarde du fichier DIMACS et renvoie le niveau de difficulte choisi
                   if(args.length!=0 && cpt==0) niv = Integer.parseInt(args[0]);
                   else niv=p.Menu();
                   
                   if(niv==1){
                       p = new Sudoku("./src/Niveau_sudoku/facile_sudoku.txt",nb_case); 
                       System.out.println("Niveau facile:");
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 729 14000");
                   }else if(niv==2){
                       p = new Sudoku("./src/Niveau_sudoku/moyen_sudoku.txt",nb_case); 
                       System.out.println("Niveau moyen:");
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 729 14000");
                   }else if(niv==3){
                       p = new Sudoku("./src/Niveau_sudoku/difficile_sudoku.txt",nb_case); 
                       System.out.println("Niveau difficile:");
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 729 14000");
                   }else if(niv==4){
                       p = new Sudoku("./src/Niveau_sudoku/diabolique_sudoku.txt",nb_case);
                       System.out.println("Niveau diabolique:");
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 729 14000"); 
                   }else if(niv==5){
                       p = new Sudoku("./src/Niveau_sudoku/demoniaque_sudoku.txt",nb_case);
                       System.out.println("Niveau demoniaque:");
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 729 14000"); 
                   }else if(niv==6){
                       nb_case=4;
                       p = new Sudoku("./src/Niveau_sudoku/4_sudoku.txt",4);
                       System.out.println("Niveau 4*4:");
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 64 27");
                       nb_case=9;
                   }else if(niv==7){
                       nb_case=4;
                       p = new Sudoku("./src/Niveau_sudoku/4_sudoku.txt",4);
                       p.Manuelle4(p.sudoku);
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 64 27");
                       nb_case=9;
                   }else if(niv==8){
                       p = new Sudoku("./src/Niveau_sudoku/demoniaque_sudoku.txt",nb_case);
                       
                       System.out.println("Entrer manuelle9Pannel");
                       Scanner sc = new Scanner(System.in);
                       int x = sc.nextInt();
                       p.Manuelle9Panel(x+"");
                       
                       
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 729 14000");
                   }else if(niv==9){
                       
                       p = new Sudoku("./src/Niveau_sudoku/4_sudoku.txt",4);
                       System.out.println("Niveau 4*4 Random:");
                       p.initGridRandom(4,9,80);
                     //p.initGridRandom(taille,niv,pourcentage de non affichage);
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 64 27");
                   }else if(niv==10){
                       
                       p = new Sudoku("./src/Niveau_sudoku/demoniaque_sudoku.txt",9);
                       System.out.println("Niveau 9*9 Random:");
                       p.initGridRandom(9,10,80);
                     //p.initGridRandom(taille,niv,pourcentage de non affichage);
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 729 14000");
                   }else if(niv==11){
                       p = new Sudoku("./src/Niveau_sudoku/16_sudoku.txt",16);
                       System.out.println("Niveau 16*16:");
                       p.initGridRandom(16,11,80);
                     //p.initGridRandom(taille,niv,pourcentage de non affichage);
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 4096 29648");
                       nb_case=16;
                   }else if(niv==12){
                       p = new Sudoku("./src/Niveau_sudoku/25_sudoku.txt",50);
                       System.out.println("Niveau 25*25:");
                       p.initGridRandom(50,12,80);
                     //p.initGridRandom(taille,niv,pourcentage de non affichage);
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 125000 31200533");//15625
                       nb_case=50;
                   }else{
                       p = new Sudoku("./src/Niveau_sudoku/facile_sudoku.txt",9); 
                       System.out.println("Niveau par defaut -> facile:");
                       niv=1;
                       p.initGridRandom(9,10,80);
                     //p.initGridRandom(taille,niv,pourcentage de non affichage);
                       p.affichage_console(p.sudoku);
                       p.sauver("./src/CNF/NIV"+niv+"_Sudoku.cnf",p.sudoku,niv,"p cnf 729 14000");
                  }
                   
                   System.out.println("---------------- Creation du fichier DIMACS    ----------------");
                   System.out.println("");
                   System.out.println("./src/CNF/NIV"+niv+"_Sudoku.cnf"); 
                   System.out.println("");
                   System.out.println("---------------- Creation du fichier solution  ----------------");
                   System.out.println("");
                   System.out.println("./src/SAT/SOL"+niv+"_Sudoku.sat");
                   System.out.println("");
                   
                   p.solution(p.sudoku,"./src/SAT/SOL"+niv+"_Sudoku.sat",niv);
                   p.affichage_console(p.sudoku);
                   
                   
                   System.out.println("Voulez-vous REJOUER('o'  ou 'O' ou '0')->");
                   rejouer = reader.next();
                   System.out.println("Rejouer ->"+rejouer.charAt(0));
                   cpt++;    
                   System.out.println("");
           }//REJOUER
       
       
       }catch (IOException io) { 
            throw new RuntimeException("Erreur: Menu Sudoku() "+io);
       }    
       
       System.out.println("Fin du main");
     }
    }

