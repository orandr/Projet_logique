WARNING: for repeatability, setting FPU to use double precision
============================[ Problem Statistics ]=============================
|                                                                             |
|  Number of variables:           729                                         |
|  Number of clauses:           13053                                         |
|  Parse time:                   0.00 s                                       |
|                                                                             |
============================[ Search Statistics ]==============================
| Conflicts |          ORIGINAL         |          LEARNT          | Progress |
|           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |
===============================================================================
===============================================================================
restarts              : 1
conflicts             : 1              (166 /sec)
decisions             : 85             (0.00 % random) (14068 /sec)
propagations          : 756            (125124 /sec)
conflict literals     : 18             (0.00 % deleted)
Memory used           : 20.00 MB
CPU time              : 0.006042 s

SATISFIABLE
